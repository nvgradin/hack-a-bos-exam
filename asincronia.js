/*
    -----------------------------------------------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------------------------------------

    * * * * * * * * * * * * * * * * * * * * * * *
    *  3. ASINCRONIA                            *
    * * * * * * * * * * * * * * * * * * * * * * *

    En este ejercicio se comprobará la competencia de los alumnos en el concepto de asincronía 
    Se proporcionan 3 archivos  csv separados por comas y se deberán bajar asíncronamente (promises) 
    
    A la salida se juntarán los registros de los 3 archivos en un array que será el parámetro de entrada 
    de la funcion findIPbyName(array, name ,surname) que buscará una entrada en el array y devolverá la IP correspondiente

    Una vez hallada la IP ha de mostrarse por pantalla para llamar a la función utilizad el nombre Cari Wederell
    -----------------------------------------------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------------------------------------
*/

const fs = require('fs');
const fsPromises = fs.promises;

const promisesData = [
    readFromFile('files/MOCK_DATA1.csv'),
    readFromFile('files/MOCK_DATA2.csv'),
    readFromFile('files/MOCK_DATA3.csv')
];

const FIRST_NAME = 1; // POSICION EN EL ARRAY DEL NOMBRE
const LAST_NAME = 2; // POSICION EN EL ARRAY DEL APELLIDO
const IP_POSITION = 5; // POSICION EN EL ARRAY DEL IP

// Crear la función que busque la IP correspondiente en función al user solicitado
// 
// He intentado configurar la entrada a la función con el array, pero no consigo
// configurar la salida de los datos conseguidos del promise para trabajar con ellas
// en la función findIPbyName. 
// Actualmente la deje desactivada para poder ver el funcionamiento de la función readFromFile.

/*
function findIPbyName(array, name, surname) {
    let userIP = {}
    for (let line of userPromise) {
        const fieldOfLine = array.split(';')
        if (line[FIRST_NAME] == name && line[LAST_NAME] == surname) {
            userIP = [IP_POSITION];
        }
    }
    console.log(userIP);
    return userIP;
}
*/

// Crear la función para bajar asíncronamente los 3 archivos csv, y darle formato como array.

function readFromFile(file) {
    let userPromise
    fsPromises.readFile(file)
        .then((data) => {
            const listUsers = data.toString()
                .split('\n')
                .filter(line => line.trim().length !== 0)
            userPromise = listUsers
                .map(user => user.split(','))
            console.log(userPromise);

            return Promise.all(userPromise)
        })
        .catch((err) => {
            console.log(err)
        })
}

// 
//console.log(findIPbyName('Cari', 'Wederell'));  /// test data