/*  -----------------------------------------------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------------------------------------

    * * * * * * * * * * * * * * * *
    *  1. C A L C U L A D O R A   *
    * * * * * * * * * * * * * * * *

    Crea un programa que permita realizar sumas, restas, multiplicaciones y divisiones. 

        - El programa debe recibir dos números (n1, n2).

        - Debe existir una variable que permita seleccionar de alguna forma el tipo de operación (suma, resta, multiplicación 
          o división).

        - Opcional: agrega una operación que permita elevar n1 a la potencia n2.

    -----------------------------------------------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------------------------------------
*/

let n1 = 0;
let n2 = 0;
let operadores = '';
let sumar = '+';

function operar(n1, n2, operadores) {
    result = 0;
    switch (operadores) {
        case '+':
            result = parseFloat(n1) + parseFloat(n2);
            break;
        case '-':
            result = parseFloat(n1) - parseFloat(n2);
            break;
        case '*':
            result = parseFloat(n1) * parseFloat(n2);
            break;
        case '/':
            result = parseFloat(n1) / parseFloat(n2);
            break;
        case '**':
            result = Math.pow(parseFloat(n1), parseFloat(n2));
            break;
        default:
            result = 'ERROR. Porfavor intentalo de nuevo. Es necesario introducir dos numeros y un operador para operar!';
            break;
    }
return result;
}

// casos de test

console.log(operar('2', '3', '+'));  // datos introducidos como string y probando la suma
console.log(operar('4.34', '3', '-'));  // datos introducidos como strin, uno con con decimales y probando la resta
console.log(operar('2', '3', '*')); //  datos introducidos como string y probando la multiplicacion
console.log(operar(12, 8, '/')); // datos introducidos como números y probando la división
console.log(operar(7, 2, '**')); // datos introducidos como números y probando elevar n1 a la potencia n2
console.log(operar()); // cadena vacía
console.log(operar(5,6,'')); // datos introducidos como números y sin operador